import datetime as dt
import decimal
import uuid
from dataclasses import dataclass
from marshmallow import Schema, fields, post_load


def sam_dc_rec(cls):
    attrDict = {}
    TYPE_MAPPING = {
        str: fields.String,
        bytes: fields.String,
        dt.datetime: fields.DateTime,
        float: fields.Float,
        bool: fields.Boolean,
        tuple: fields.Raw,
        list: fields.Raw,
        set: fields.Raw,
        int: fields.Integer,
        uuid.UUID: fields.UUID,
        dt.time: fields.Time,
        dt.date: fields.Date,
        dt.timedelta: fields.TimeDelta,
        decimal.Decimal: fields.Decimal,
    }
    for key, attr in cls.__annotations__.items():
        try:
            attr.__annotations__
            nestAttr = {}
            for k, attr in attr.__annotations__.items():
                nestAttr[k] = TYPE_MAPPING[attr]()
            attrDict[key] = fields.Nested(
              type(attr.__name__,(Schema,),nestAttr))
        except AttributeError:
            attrDict[key] = TYPE_MAPPING[attr]()
  

    my_dc = dataclass(cls)
    def myLoad(self, data, **kwargs):
      return my_dc(**data)

    attrDict['my_load'] = post_load(myLoad)
    my_dc.Schema = type(cls.__name__, (Schema,), attrDict)
    return my_dc


@dataclass
class MySam():
    name: str
    age: int


@sam_dc_rec
@dataclass
class MySam2():
    sam: MySam
    weight: int


print(MySam2
.Schema()
.load(
  {'sam': {'name': 'kim', 'age': 100}, 'weight': 100}))
